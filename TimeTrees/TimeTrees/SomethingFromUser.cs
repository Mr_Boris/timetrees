﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class SomethingFromUser
    {
        //3 метода, которые проверяют корректность даты, полученной от пользователя
        // //back для возможности вернуться в меню
        public static string DeathDate(string birthDate)
        {
            bool isBirthDateLessDeathDate;
            bool isFormatCorrect;
            bool isDateLessThenNow;
            string deathDate;
            do
            {
                Console.WriteLine("Введите дату смерти");
                deathDate = Console.ReadLine();
                if ((deathDate == "") || (deathDate == "//back")) return deathDate;

                isFormatCorrect = IsDateCorrect(deathDate);
                if (!isFormatCorrect)
                    Console.WriteLine("Некорректный формат даты");

                isDateLessThenNow = (DateParser.DoDateCorrect(deathDate).CompareTo(DateTime.Now) < 0);
                if (isFormatCorrect && !(isDateLessThenNow))
                    Console.WriteLine("Ошибка! Введённая дата ещё не наступила");

                isBirthDateLessDeathDate = DateParser.DoDateCorrect(birthDate).CompareTo(DateParser.DoDateCorrect(deathDate)) < 0;
                if (isFormatCorrect && !isBirthDateLessDeathDate)
                    Console.WriteLine("Ошибка! Дата смерти не может быть меньше даты рождения");


            } while (!isDateLessThenNow || !isBirthDateLessDeathDate || !isFormatCorrect);
            return deathDate;
        }

        public static string Date()
        {
            bool isFormatCorrect;
            string someDate;
            do
            {
                someDate = Console.ReadLine();
                if (someDate == "//back") return someDate;
                isFormatCorrect = IsDateCorrect(someDate);
                if (!isFormatCorrect)
                    Console.WriteLine("Некоректный формат даты, попробуйте ввести дату ещё раз");
                if ((isFormatCorrect) && !(DateParser.DoDateCorrect(someDate).CompareTo(DateTime.Now) < 0))
                    Console.WriteLine("Ошибка! Введённая дата ещё не наступила, попробуйте ввести дату ещё раз");
            } while ((!isFormatCorrect) || !(DateParser.DoDateCorrect(someDate).CompareTo(DateTime.Now) < 0));
            return someDate;
        }

        public static bool IsDateCorrect(string someDate)
        {
            //есть ли между цифрами тире
            string[] date = someDate.Split('-');
            int[] testArray = new int[date.Length];
            try
            {
                for (int i = 0; i < testArray.Length; i++)
                {
                    //являются ли введённые значения цифрами
                    testArray[i] = int.Parse(date[i]);
                }
            }
            catch
            {
                return false;
            }
            //Год должен состоять из 3 или 4 цифр //В дате не более 3 элементов
            return (!(date[0].Length <= 2 || date[0].Length >= 5) && !(date.Length > 3) &&
                !((testArray.Length >= 2) && (testArray[1] > 12 || testArray[1] == 0) && //Месяц не более 12-ого
                    testArray.Length == 3 && (testArray[2] > 31 || testArray[2] == 0))); //День не более 31-ого
        }
    }
}
