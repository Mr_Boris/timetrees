﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class DateParser
    {
        //Переводим из строки в дату
        //Если строка была только из года добавляем к ней месяц
        //Иначе не будет парситься
        public static DateTime DoDateCorrect(string someDate)
        {
            if (someDate.Length <= 4)
                someDate += "-01";
            return DateTime.Parse(someDate);
        }
    }
}
