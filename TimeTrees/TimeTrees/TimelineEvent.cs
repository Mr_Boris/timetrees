﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class TimelineEvent
    {
        public DateTime Date { get; set; }
        public string NameOfEvent { get; set; }
        public List<Person> Participants { get; set; }
    }
}
